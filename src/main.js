import { createApp } from 'vue';
import App from './App.vue';
import './registerServiceWorker';
import router from '@/router';
import store from './store';
import '@/assets/output.css';
import VueGAPI from "vue-gapi"

const apiConfig = {
  apiKey: "AIzaSyADmz8Sr5BG1ezUhfPeSrrG1iEJQAOYrxg",
  clientId: "965962171082-ajfhsvuo3bf9rrdfr9t337dh17dlvd5l.apps.googleusercontent.com",
  discoveryDocs: ["https://sheets.googleapis.com/$discovery/rest?version=v4"],
  scope: "https://www.googleapis.com/auth/spreadsheets" 
};

createApp(App).use(store).use(router).use(VueGAPI, apiConfig).mount('#app');
