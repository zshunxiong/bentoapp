import { createRouter, createWebHistory } from 'vue-router'
import Home from '@/components/pages/Home.vue'

export const routes = [
  {
    path: '/',
    name: 'Home',
    text: '轉換程式',
    component: Home
  },
  {
    path: '/record',
    name: 'Record',
    text: '紀錄',
    component: () => import('@/components/pages/Record.vue')
  },
  // {
  //   path: '/bento',
  //   name: 'Bento',
  //   text: '便當v2.0 (測試中)',
  //   component: () => import('@/components/pages/Bento.vue'),
  //   children: [
  //     {
  //       path: '',
  //       component: () => import('@/components/pages/bento/Shop.vue'),
  //     },
  //     {
  //       path: 'order',
  //       component: () => import('@/components/pages/Bento.vue'),
  //     }
  //   ]
  // },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
