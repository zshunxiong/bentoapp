module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? '/bentoapp/' : '/',
  // publicPath: process.env.NODE_ENV === 'production' ? '/' : '/',
  pwa: {
    name: '便當助手',
    themeColor: "#f4a986",
    msTileColor: "#f4a986",
    appleMobileWebAppCapable: 'yes',
    iconPaths: {
      favicon32: null,
      favicon16: null,
      appleTouchIcon: null,
      maskIcon: null,
      msTileImage: null
    },
    manifestOptions: {
      background_color: "#f4a986",
      icons: [
        {
          "src": "./img/icons/manifest-icon-192.maskable.png",
          "sizes": "192x192",
          "type": "image/png",
          "purpose": "any"
        },
        {
          "src": "./img/icons/manifest-icon-192.maskable.png",
          "sizes": "192x192",
          "type": "image/png",
          "purpose": "maskable"
        },
        {
          "src": "./img/icons/manifest-icon-512.maskable.png",
          "sizes": "512x512",
          "type": "image/png",
          "purpose": "any"
        },
        {
          "src": "./img/icons/manifest-icon-512.maskable.png",
          "sizes": "512x512",
          "type": "image/png",
          "purpose": "maskable"
        }
      ],
    },
    workboxOptions: {
      skipWaiting: true
    }
  }
};